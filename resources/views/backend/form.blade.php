@extends('layouts.app')

@section('content')
    @if (isset($exists))
        <form-component formdata='@json($exists)'></form-component>
    @else
        <form-component formdata="null"></form-component>
    @endif
@endsection
