@extends('layouts.app')

@section('content')
    <landing-page formdata='@json($formdata)' img="{{ $img }}"></landing-page>
@endsection
