import axios from 'axios';

const baseURL = process.env.MIX_API_URL

const custom_axios = axios.create({
    baseURL: baseURL,
});

export default custom_axios