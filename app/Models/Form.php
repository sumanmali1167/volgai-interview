<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Form extends Model
{
    use HasFactory;

    protected $fillable = array(
        "heading",
        "link",
        "linkedin",
        "instagram",
        "facebook",
        "color",
        "logo",
    );

    public function getLogoAttribute($value)
    {
        $path = 'images/logo/' . $value;
        return Storage::disk('public')->url($path);
    }
}
