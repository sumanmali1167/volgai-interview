<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Http;

class UnsplashImage
{

    protected $title, $key, $url;

    public function __construct()
    {
        $this->key = config('services.unsplash.key');
        $this->url = config('services.unsplash.url');
    }

    public function getImage($query)
    {
        $url = $this->url . '/photos/random?query=' . $query;

        $response = Http::withHeaders(['Authorization' => 'Client-ID ' . $this->key])->get($url);

        if ($response->status() == 200) {
            $img = json_decode($response->body());
            return ['status' => $response->status(), 'data' => $img->urls->regular];
        } else
            return ['status' => $response->status(), 'data' => ''];
    }
}
