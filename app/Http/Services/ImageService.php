<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageService
{
    public function getFileName($path)
    {
        $arr = explode('/', $path);
        return $arr[count($arr) - 1];
    }

    public function store($name, $file, $path)
    {
        // return $file;
        $name =  Str::of($name)->slug('-') . '.' . $file->getClientOriginalExtension();
        Storage::disk('public')->putFileAs($path, $file, $name);
        return $name;
    }

    public function destroy($path)
    {
        Storage::disk('public')->delete($path);
    }
}
