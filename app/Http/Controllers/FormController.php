<?php

namespace App\Http\Controllers;

use App\Http\Services\ImageService;
use App\Http\Services\UnsplashImage;
use App\Models\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FormController extends Controller
{
    protected $unsplashService, $imgService;

    public function __construct(UnsplashImage $service, ImageService $imgService)
    {
        $this->unsplashService = $service;
        $this->imgService = $imgService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $formdata = Form::select(
            'id',
            "heading",
            "link",
            "linkedin",
            "instagram",
            "facebook",
            "color",
            "logo",
        )->first();

        $img = config('app.url') . '/images/bg.jpg';
        if (isset($formdata)) {
            $response = $this->unsplashService->getImage($formdata->heading);
            if ($response['status'] == 200) {
                $img = $response['data'];
            }
        } else {
            $formdata = null;
        }

        return view('welcome', compact('formdata', 'img'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $exists = Form::first();
        if (isset($exists)) {
            return view('backend.form', compact('exists'));
        }
        return view('backend.form');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->hasFile('logo')) {
            $path = 'images/logo';
            $name = $this->imgService->store('logo', $request->file('logo'), $path);
        }
        Form::create($request->except('logo') + ['logo' => $name]);
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Form $form)
    {
        if ($request->hasFile('logo')) {
            $this->imgService->destroy($form->logo);
            $path = 'images/logo';
            $name = $this->imgService->store('logo', $request->file('logo'), $path);
        } else {
            $name = $form->logo;
        }
        $form->update($request->except('logo') + ['logo' => $name]);
        return redirect('/');
    }
}
