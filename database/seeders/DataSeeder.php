<?php

namespace Database\Seeders;

use App\Models\Form;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Form::create([
            "heading" => "Tour Poor Digital Presence Makes You Lose Clients",
            "logo" => "logo.png",
            "color" => "yellow",
            "link" => "flintt.com.au",
            "linkedin" => "/flintt",
            "instagram" => "@flinttautomated",
            "facebook" => "@flinttautomated",
        ]);
    }
}
